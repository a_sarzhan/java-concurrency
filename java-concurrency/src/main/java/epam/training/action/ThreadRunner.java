package epam.training.action;

import epam.training.entity.Matrix;
import epam.training.entity.MatrixThread;
import epam.training.utility.TextFileReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ThreadRunner {
    private List<Thread> threadList = new ArrayList<>();
    private static File threadFile;
    private static final int MAXIMUM_THREAD_AMOUNT = 6;
    private static final int MINIMUM_THREAD_AMOUNT = 4;
    private static final Logger logger = LogManager.getLogger(ThreadRunner.class);
    private int threadAmount;
    private Matrix matrix;

    {
        threadAmount = new Random().nextInt(MAXIMUM_THREAD_AMOUNT - MINIMUM_THREAD_AMOUNT) + MINIMUM_THREAD_AMOUNT;
        URL address = this.getClass().getClassLoader().getResource("thread_numbers.txt");
        try {
            threadFile = new File(address.toURI());
        } catch (URISyntaxException e) {
            logger.error("Error occurred while getting file with thread numbers. Exception details: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public ThreadRunner(Matrix matrix) {
        this.matrix = matrix;
    }

    public List<Thread> getThreadList() {
        return threadList;
    }

    private List<Integer> getPositiveThreadUniqueNumbersList() {
        List<Integer> unfilteredNumbersList = TextFileReader.getListOfValuesFromFile(threadFile);
        unfilteredNumbersList.removeIf(n -> (n <= 0));
        unfilteredNumbersList = unfilteredNumbersList.stream().distinct().collect(Collectors.toList());

        logger.info("Filtering unique and positive numbers for threads succeeded");
        return unfilteredNumbersList;
    }

    private void addThreadsToList() {
        MatrixThread thread;
        int threadNumber;
        List<Integer> threadNumbersList = getPositiveThreadUniqueNumbersList();

        for (int i = 0; i < threadAmount; i++) {
            try {
                threadNumber = threadNumbersList.get(i);
                thread = new MatrixThread("Thread_" + threadNumber, threadNumber, matrix);
                threadList.add(thread);
            } catch (IndexOutOfBoundsException e) {
                logger.error("Error occurred while creating thread objects.\nLack of thread numbers in the file ["
                        + threadFile.getName() + "]. Thread Amount = " + threadAmount + " \n" + e.getMessage());
                break;
            }
        }
        logger.info("All initialized threads successfully added to List.");
    }

    public void runMatrixThreads() {
        addThreadsToList();
        logger.info("Running threads from List have started");
        for (Thread thread : threadList) {
            thread.start();
        }
    }
}

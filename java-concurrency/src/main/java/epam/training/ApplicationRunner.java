package epam.training;

import epam.training.action.ThreadRunner;
import epam.training.entity.Matrix;

import java.util.concurrent.TimeUnit;

/**
 * Epam - Java Web Development Training
 * Task - Java Concurrency
 * Assel Sarzhanova
 */

public class ApplicationRunner {

    public static void main(String[] args) {
        Matrix matrix = new Matrix();
        matrix.initializeMatrixNotDiagonalElements();
        matrix.printMatrixArray();

        ThreadRunner threadRunner = new ThreadRunner(matrix);
        threadRunner.runMatrixThreads();

        try {
            TimeUnit.SECONDS.sleep(2);
            System.out.println();
            matrix.printMatrixArray();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

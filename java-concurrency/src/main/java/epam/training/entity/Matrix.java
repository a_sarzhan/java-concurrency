package epam.training.entity;

import epam.training.utility.TextFileReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class Matrix {
    private static final int MATRIX_MAXIMUM_SIZE = 12;
    private static final int MATRIX_MINIMUM_SIZE = 8;
    private static final Logger logger = LogManager.getLogger(Matrix.class);
    private static File matrixFile;
    private static ReentrantLock locking = new ReentrantLock();
    private static AtomicInteger index = new AtomicInteger(0);
    private int[][] matrixArray;

    {
        int size = new Random().nextInt(MATRIX_MAXIMUM_SIZE - MATRIX_MINIMUM_SIZE) + MATRIX_MINIMUM_SIZE;
        matrixArray = new int[size][size];
        URL address = this.getClass().getClassLoader().getResource("matrix_values.txt");
        try {
            matrixFile = new File(address.toURI());
        } catch (URISyntaxException e) {
            logger.error("Error occurred while getting file with matrix values. Exception details: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public int[][] getMatrixArray() {
        return matrixArray;
    }

    public void initializeMatrixNotDiagonalElements() {
        List<Integer> matrixValuesList = TextFileReader.getListOfValuesFromFile(matrixFile);
        int listIndex = 0;
        for (int i = 0; i < matrixArray.length; i++) {
            try {
                for (int j = 0; j < matrixArray[i].length; j++) {
                    if (i != j) {
                        matrixArray[i][j] = matrixValuesList.get(listIndex);
                        listIndex++;
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                logger.error("Not enough numbers to initialize matrix in the file [" + matrixFile.getName() + "]\n"
                        + e.getMessage());
                break;
            }
        }
        logger.info("Matrix none diagonal elements initialized by valid numbers from the file ["
                + matrixFile.getName() + "].");
    }

    void fillMatrixMainDiagonal(int threadNumber) {
        String outputString;
        int i = 0;
        while (index.get() < matrixArray.length) {
            if (locking.tryLock()) {
                try {
                    i = index.getAndIncrement();
                    matrixArray[i][i] = threadNumber;
                    outputString = String.format("%s changed matrix[%d][%d] = %d",
                            Thread.currentThread().getName(), i, i, threadNumber);
                    logger.debug(outputString);
                } finally {
                    locking.unlock();
                }
            }
            try {
                TimeUnit.MILLISECONDS.sleep(20);
            } catch (InterruptedException e) {
                logger.error(Thread.currentThread().getName() + " has been interrupted. " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void printMatrixArray() {
        for (int i = 0; i < matrixArray.length; i++) {
            for (int j = 0; j < matrixArray[i].length; j++) {
                System.out.printf("%5d", matrixArray[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

}

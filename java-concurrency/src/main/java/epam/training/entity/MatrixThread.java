package epam.training.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class MatrixThread extends Thread {
    private final int uniqueThreadNumber;
    private static final Logger logger = LogManager.getLogger(MatrixThread.class);
    private Matrix matrix;

    public MatrixThread(String name, int threadNumber, Matrix matrix) {
        super(name);
        this.uniqueThreadNumber = threadNumber;
        this.matrix = matrix;
    }

    public int getUniqueThreadNumber() {
        return uniqueThreadNumber;
    }

    @Override
    public void run() {
        try {
            logger.info(this.getName() + " has started!");
            matrix.fillMatrixMainDiagonal(uniqueThreadNumber);
            TimeUnit.MILLISECONDS.sleep(50);
            logger.info(this.getName() + " has finished");
        } catch (InterruptedException e) {
            logger.error(this.getName() + " has been interrupted. " + e.getMessage());
            e.printStackTrace();
        }
    }

}

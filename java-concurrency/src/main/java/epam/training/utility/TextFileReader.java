package epam.training.utility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TextFileReader {
    private static final Logger logger = LogManager.getLogger(TextFileReader.class);

    public static List<Integer> getListOfValuesFromFile(File file) {
        List<Integer> valuesList = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            List<String> fileDataByLineList = reader.lines().collect(Collectors.toList());
            valuesList = getFilteredFileNumbersInList(fileDataByLineList, file);

            if (fileDataByLineList.isEmpty()) {
                logger.error("File [" + file.getName() + "] is empty");
            } else {
                logger.info("Data from file [" + file.getName() + "] successfully added to List");
            }
        } catch (IOException e) {
            logger.error("Error occurred while opening/reading file [" + file.getName() + "]. " + e.getMessage());
            e.printStackTrace();
        }

        return valuesList;
    }

    private static List<Integer> getFilteredFileNumbersInList(List<String> fileData, File file) {
        String[] stringValuesArray;
        List<Integer> numbersList = new ArrayList<>();

        for (String dataByLine : fileData) {
            stringValuesArray = dataByLine.split("\\s+");
            for (String stringValue : stringValuesArray) {
                try {
                    numbersList.add(Integer.parseInt(stringValue));
                } catch (NumberFormatException e) {
                    logger.info("Found value = " + stringValue + " with wrong number format in the file [" + file.getName() + "].");
                    e.printStackTrace();
                }
            }
        }
        logger.info("Sorting file [" + file.getName() + "] content for valid numbers succeeded");
        return numbersList;
    }

}
